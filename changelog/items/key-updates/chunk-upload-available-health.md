- Add `uploadChunkAvailableHealth` to be able to tweak at what point the chunk
  is considered available.
