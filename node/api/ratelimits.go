package api

import (
	"net/http"
	"sync"

	"gitlab.com/NebulousLabs/ratelimit"
	"gitlab.com/SkynetLabs/skyd/build"
)

// ratelimitedResponseWriter is a type to wrap a response writer to limit its
// throughput.
type ratelimitedResponseWriter struct {
	staticRL *ratelimit.RateLimit
	staticW  http.ResponseWriter
}

// newRatelimitedResponseWriter creates a new ratelimited response writer.
func newRatelimitedResponseWriter(w http.ResponseWriter, rl *ratelimit.RateLimit) http.ResponseWriter {
	return &ratelimitedResponseWriter{
		staticRL: rl,
		staticW:  w,
	}
}

// Header forwards the call to the internal response writer.
func (rrw *ratelimitedResponseWriter) Header() http.Header {
	return rrw.staticW.Header()
}

// Write wraps the response writer in a ratelimit and then forwards the data to
// the wrapped writer's call method.
func (rrw *ratelimitedResponseWriter) Write(b []byte) (int, error) {
	rw := ratelimit.NewRLReadWriter(&writeReader{rrw.staticW}, rrw.staticRL, make(<-chan struct{}))
	return rw.Write(b)
}

// WriteHeader forwards the call to the internal response writer.
func (rrw *ratelimitedResponseWriter) WriteHeader(statusCode int) {
	rrw.staticW.WriteHeader(statusCode)
}

// activeRatelimit describes a ratelimit that is currently being used by one or
// more parallel requests.
type activeRatelimit struct {
	refcount int
	staticRL *ratelimit.RateLimit
}

// rateLimits contains all active connections.
type rateLimits struct {
	activeDownloads map[string]*activeRatelimit
	mu              sync.Mutex
}

// newRatelimits creates a new rateLimits.
func newRatelimits() *rateLimits {
	return &rateLimits{
		activeDownloads: make(map[string]*activeRatelimit),
	}
}

// LimitDownload wraps a response writer in a ratelimited response writer for
// the user with the given uid. When called multiple times with the same uid,
// the calls will share the same ratelimit.
func (r *rateLimits) LimitDownload(w http.ResponseWriter, uid string, limit int64) (http.ResponseWriter, func()) {
	r.mu.Lock()
	defer r.mu.Unlock()
	activeLimit, exists := r.activeDownloads[uid]
	if !exists {
		var rl *ratelimit.RateLimit
		if limit == 0 {
			rl = ratelimit.NewRateLimit(0, 0, 0)
		} else {
			rl = ratelimit.NewRateLimit(0, limit, RatelimitPacketSize)
		}
		activeLimit = &activeRatelimit{
			staticRL: rl,
		}
		r.activeDownloads[uid] = activeLimit
	}
	activeLimit.refcount++

	decrease := func() {
		r.managedReturnDownload(uid)
	}
	return newRatelimitedResponseWriter(w, activeLimit.staticRL), decrease
}

// managedReturnDownload is used to decrement the refcount of an active download
// after it's done. This should never be called directly but instead be returned
// by LimitDownload.
func (r *rateLimits) managedReturnDownload(uid string) {
	r.mu.Lock()
	defer r.mu.Unlock()
	activeLimit, exists := r.activeDownloads[uid]
	if !exists {
		build.Critical("managedReturnDownload: unknown uid")
		return
	}
	activeLimit.refcount--
	if activeLimit.refcount < 0 {
		build.Critical("managedReturnDownload: negative refcount", activeLimit.refcount)
	}
	if activeLimit.refcount <= 0 {
		delete(r.activeDownloads, uid)
	}
}
