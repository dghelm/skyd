package skynet

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/eventials/go-tus"
	"gitlab.com/NebulousLabs/errors"
	"gitlab.com/NebulousLabs/fastrand"
	"gitlab.com/SkynetLabs/skyd/build"
	"gitlab.com/SkynetLabs/skyd/node"
	"gitlab.com/SkynetLabs/skyd/node/api/client"
	"gitlab.com/SkynetLabs/skyd/siatest"
	"gitlab.com/SkynetLabs/skyd/siatest/dependencies"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"gitlab.com/SkynetLabs/skyd/skymodules/renter"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.sia.tech/siad/crypto"
	"go.sia.tech/siad/modules"
)

// tusEndpoint is the endpoint for all tus related requests.
const tusEndpoint = "/skynet/tus"

// mongoTestCreds are the credentials for the test mongodb.
var mongoTestCreds = options.Credential{
	Username: "root",
	Password: "pwd",
}

// partialUpload is a helper type which consists of the URL of a partial upload
// which is specific to the portal it was created on.
type partialUpload url.URL

// newPartialUpload creates a new partialUpload object from
func newPartialUpload(uploadURL string) (*partialUpload, error) {
	url, err := url.Parse(uploadURL)
	if err != nil {
		return nil, err
	}
	return (*partialUpload)(url), nil
}

// ID returns the uploads ID.
func (pu *partialUpload) ID() string {
	return filepath.Base(pu.Path)
}

// Patch adds some data to a partial upload.
func (pu *partialUpload) Patch(data []byte, offset uint64) error {
	req, err := http.NewRequest("PATCH", pu.URL(), bytes.NewReader(data))
	if err != nil {
		return err
	}
	req.Header.Set("Tus-Resumable", "1.0.0")
	req.Header.Set("Upload-Offset", fmt.Sprint(offset))
	req.Header.Set("Content-Type", "application/offset+octet-stream")

	_, err = request(req)
	return err
}

// URL returns the url of the upload as a string.
func (pu *partialUpload) URL() string {
	return fmt.Sprintf("%v://%v%v", pu.Scheme, pu.Host, pu.Path)
}

// request is a helper function to send a http request, check for an
// error and return the response header.
func request(req *http.Request) (http.Header, error) {
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode <= 200 || resp.StatusCode > 299 {
		return nil, fmt.Errorf("error status code: %v", resp.StatusCode)
	}
	return resp.Header, nil
}

// createPartialUpload creates a new partial upload on a portal.
func createPartialUpload(r *siatest.TestNode, size uint64) (*partialUpload, error) {
	req, err := r.NewRequest("POST", tusEndpoint, bytes.NewReader(nil))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Tus-Resumable", "1.0.0")
	req.Header.Set("SkynetMaxUploadSize", fmt.Sprint(10*modules.SectorSize))
	req.Header.Set("Upload-Length", fmt.Sprint(size))
	req.Header.Set("Upload-Concat", "partial")

	h, err := request(req)
	if err != nil {
		return nil, err
	}
	url, err := url.Parse(h.Get("Location"))
	return (*partialUpload)(url), err
}

// concatPartialUploads concatenates some partial uploads into a single one.
func concatPartialUploads(r *siatest.TestNode, upload1, upload2 *partialUpload) (string, error) {
	req, err := r.NewRequest("POST", tusEndpoint, bytes.NewReader(nil))
	if err != nil {
		return "", err
	}
	req.Header.Set("Tus-Resumable", "1.0.0")
	req.Header.Set("Upload-Concat", fmt.Sprintf("final;%s %s", upload1.ID(), upload2.ID()))
	req.Header.Set("SkynetMaxUploadSize", fmt.Sprint(10*modules.SectorSize))

	h, err := request(req)
	if err != nil {
		return "", err
	}
	url, err := url.Parse(h.Get("Location"))
	if err != nil {
		return "", err
	}
	return filepath.Base(url.Path), nil
}

// TestSkynetTUSUploader runs all skynetTUSUploader related tests.
func TestSkynetTUSUploader(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	t.Parallel()

	// Helper to run tests.
	runTests := func(t *testing.T, mongo bool) {
		// Prepare a testgroup.
		groupParams := siatest.GroupParams{
			Hosts:  3,
			Miners: 1,
		}
		groupDir := skynetTestDir(t.Name())
		tg, err := siatest.NewGroupFromTemplate(groupDir, groupParams)
		if err != nil {
			t.Fatal(err)
		}
		defer func() {
			if err := tg.Close(); err != nil {
				t.Fatal(err)
			}
		}()

		// Create a mongo test store if necessary.
		rt := node.RenterTemplate
		if mongo {
			uri, ok := build.MongoDBURI()
			if !ok {
				build.Critical("uri not set")
			}
			rt.MongoUploadStoreURI = uri
			rt.MongoUploadStoreCreds = mongoTestCreds
			rt.MongoUploadStoreServerUID = t.Name()
		}
		if _, err := tg.AddNodes(rt); err != nil {
			t.Fatal(err)
		}

		// Run tests.
		t.Run("PruneIdle", func(t *testing.T) {
			testTUSUploaderPruneIdle(t, tg.Renters()[0]) // first to avoid ndf
		})
		t.Run("Basic", func(t *testing.T) {
			testTUSUploaderBasic(t, tg.Renters()[0])
		})
		t.Run("Options", func(t *testing.T) {
			testOptionsHandler(t, tg.Renters()[0])
		})
		t.Run("Concat", func(t *testing.T) {
			testTUSUploaderConcat(t, tg.Renters()[0])
		})
		if mongo {
			// Can only run on a distributed DB.
			t.Run("ConcatSeparatePortals", func(t *testing.T) {
				testTUSUploaderConcatSeparateServers(t, tg, rt)
			})
		}
		t.Run("TooLarge", func(t *testing.T) {
			testTUSUploaderTooLarge(t, tg.Renters()[0])
		})
		t.Run("Trustless", func(t *testing.T) {
			testTrustlessUpload(t, tg)
		})
		t.Run("UnstableConnection", func(t *testing.T) {
			testTUSUploaderUnstableConnection(t, tg)
		})
		t.Run("DroppedConnection", func(t *testing.T) {
			testTUSUploaderConnectionDropped(t, tg)
		})
	}

	// Run with in-memory db.
	t.Run("InMemory", func(t *testing.T) {
		runTests(t, false)
	})

	// Run with mongodb.
	t.Run("Mongo", func(t *testing.T) {
		runTests(t, true)
	})
}

// testTUSUploadBasic tests uploading multiple files using the TUS protocol and
// verifies that pruning doesn't delete completed .sia files.
func testTUSUploaderBasic(t *testing.T, r *siatest.TestNode) {
	// Get the number of files before the test.
	dir, err := r.RenterDirRootGet(skymodules.SkynetFolder)
	if err != nil {
		t.Fatal(err)
	}
	nFilesBefore := dir.Directories[0].AggregateNumFiles

	// Declare the chunkSize.
	chunkSize := 2 * int64(skymodules.ChunkSize(crypto.TypePlain, uint64(skymodules.RenterDefaultDataPieces)))

	// Declare a test helper that uploads a file and downloads it.
	uploadTest := func(fileSize int64, baseSectorRedundancy, fanoutHealth float64) error {
		uploadedData := fastrand.Bytes(int(fileSize))
		fileName := hex.EncodeToString(fastrand.Bytes(10))
		fileType := hex.EncodeToString(fastrand.Bytes(10))
		skylink, err := r.SkynetTUSUploadFromBytes(uploadedData, chunkSize, fileName, fileType)
		if err != nil {
			return err
		}
		var sl skymodules.Skylink
		err = sl.LoadString(skylink)
		if err != nil {
			return err
		}

		// Wait for the upload to reach full health.
		err = build.Retry(100, 100*time.Millisecond, func() error {
			shg, err := r.SkylinkHealthGET(sl)
			if err != nil {
				return err
			}
			if shg.BaseSectorRedundancy != uint64(baseSectorRedundancy) || shg.FanoutEffectiveRedundancy != fanoutHealth {
				return fmt.Errorf("wrong health %v %v", shg.BaseSectorRedundancy, shg.FanoutEffectiveRedundancy)
			}
			return nil
		})
		if err != nil {
			return err
		}

		// Download the uploaded data and compare it to the uploaded data.
		downloadedData, err := r.SkynetSkylinkGet(skylink)
		if err != nil {
			return err
		}
		_, sm, err := r.SkynetMetadataGet(skylink)
		if err != nil {
			return err
		}
		if !bytes.Equal(uploadedData, downloadedData) {
			return errors.New("data doesn't match")
		}
		if sm.Length != uint64(len(uploadedData)) {
			return errors.New("wrong length in metadata")
		}
		if sm.Filename != fileName {
			t.Fatalf("Invalid filename %v != %v", sm.Filename, fileName)
		}
		if len(sm.Subfiles) != 1 {
			t.Fatal("expected one subfile but got", len(sm.Subfiles))
		}
		ssm, exists := sm.Subfiles[fileName]
		if !exists {
			t.Fatal("subfile missing")
		}
		if ssm.Filename != sm.Filename {
			t.Fatal("filename mismatch")
		}
		if ssm.Len != sm.Length {
			t.Fatal("length mismatch")
		}
		if ssm.Offset != 0 {
			t.Fatal("offset should be zero")
		}
		if ssm.ContentType != fileType {
			t.Fatalf("wrong content-type %v != %v", ssm.ContentType, fileType)
		}
		return nil
	}

	// Upload a large file.
	if err := uploadTest(chunkSize*5+chunkSize/2, 2, 3); err != nil {
		t.Fatal(err)
	}

	// Upload a byte that's smaller than a sector but still a large file.
	if err := uploadTest(int64(modules.SectorSize)-1, 2, 3); err != nil {
		t.Fatal(err)
	}

	// Upload a small file.
	if err := uploadTest(1, 2, 0); err != nil {
		t.Fatal(err)
	}

	// Upload empty file.
	if err := uploadTest(0, 2, 0); err != nil {
		t.Fatal(err)
	}

	// Set the max size to 1 chunkSize.
	err = os.Setenv("TUS_MAXSIZE", fmt.Sprint(chunkSize))
	if err != nil {
		t.Fatal(err)
	}

	// Restart the renter for the change to take effect.
	err = r.RestartNode()
	if err != nil {
		t.Fatal(err)
	}

	// Upload file that is too large.
	if err := uploadTest(2*chunkSize, 0, 0); err == nil || !strings.Contains(err.Error(), "upload body is to large") {
		t.Fatal(err)
	}

	// Reset size.
	err = os.Unsetenv("TUS_MAXSIZE")
	if err != nil {
		t.Fatal(err)
	}

	// Restart the renter again.
	err = r.RestartNode()
	if err != nil {
		t.Fatal(err)
	}

	// Wait for two full pruning intervals to make sure pruning ran at least
	// once.
	time.Sleep(2 * renter.PruneTUSUploadTimeout)

	// Check that the number of files increased by 6. One for the small and zero
	// uploads and 2 for each of the large ones.
	dir, err = r.RenterDirRootGet(skymodules.SkynetFolder)
	if err != nil {
		t.Fatal(err)
	}
	nFiles := dir.Directories[0].AggregateNumFiles
	if nFiles-nFilesBefore != 6 {
		t.Fatal("expected 6 new files but got", nFiles-nFilesBefore)
	}
}

// testTUSUploaderTooLarge tests the user specified max size of the TUS
// endpoints.
func testTUSUploaderTooLarge(t *testing.T, r *siatest.TestNode) {
	// Declare the chunkSize and data.
	chunkSize := 2 * int64(skymodules.ChunkSize(crypto.TypePlain, uint64(skymodules.RenterDefaultDataPieces)))
	data := fastrand.Bytes(int(chunkSize))

	// Upload with a max size that equals the uploaded data. This should work.
	_, err := r.SkynetTUSUploadCustom(data, chunkSize, "success", "", int64(len(data)), nil)
	if err != nil {
		t.Fatal(err)
	}

	// Upload with a max size that is 1 byte smaller than the file's size. This should fail.
	_, err = r.SkynetTUSUploadCustom(data, chunkSize, "failure", "", int64(len(data))-1, nil)
	if err == nil {
		t.Fatal(err)
	}
}

// testOptionsHandler makes sure that the tus endpoints set the expected header
// when requesting them with the OPTIONS request type.
func testOptionsHandler(t *testing.T, r *siatest.TestNode) {
	testEndpoint := func(url string) {
		req, err := http.NewRequest("OPTIONS", url, nil)
		if err != nil {
			t.Fatal(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal(err)
		}
		if err := resp.Body.Close(); err != nil {
			t.Fatal(err)
		}
		extensionHeader, ok := resp.Header["Tus-Extension"]
		if !ok {
			t.Fatal("missing header")
		}
		extensions := strings.Split(extensionHeader[0], ",")
		if len(extensions) != 3 {
			t.Fatal("wrong number of extensions", len(extensions), extensions)
		}
		if extensions[0] != "creation" {
			t.Fatal("extension 'creation' should be enabled", extensions[0])
		}
		if extensions[1] != "creation-with-upload" {
			t.Fatal("extension 'creation-with-upload' should be enabled", extensions[1])
		}
		if extensions[2] != "concatenation" {
			t.Fatal("extension 'concatenation' should be enabled", extensions[2])
		}
		if _, ok := resp.Header["Tus-Resumable"]; !ok {
			t.Fatal("missing header")
		}
		if _, ok := resp.Header["Tus-Version"]; !ok {
			t.Fatal("missing header")
		}
	}

	// Test /skynet/tus
	testEndpoint(fmt.Sprintf("http://%s/skynet/tus", r.APIAddress()))

	// Create an uploader to get an upload id.
	chunkSize := 2 * int64(skymodules.ChunkSize(crypto.TypePlain, uint64(skymodules.RenterDefaultDataPieces)))
	fileSize := chunkSize*5 + chunkSize/2 // 5 1/2 chunks.
	uploadedData := fastrand.Bytes(int(fileSize))
	tc, upload, err := r.SkynetTUSNewUploadFromBytes(uploadedData, chunkSize)
	if err != nil {
		t.Fatal(err)
	}
	uploader, err := tc.CreateUpload(upload)
	if err != nil {
		t.Fatal(err)
	}

	// Test /skynet/tus/:id
	testEndpoint(uploader.Url())
}

// testTUSUploaderPruneIdle checks that incomplete uploads get pruned after a
// while and have their .sia files deleted from disk.
func testTUSUploaderPruneIdle(t *testing.T, r *siatest.TestNode) {
	// Get the number of files before the test.
	dir, err := r.RenterDirRootGet(skymodules.SkynetFolder)
	if err != nil {
		t.Fatal(err)
	}
	nFilesBefore := dir.Directories[0].AggregateNumFiles
	if nFilesBefore != 0 {
		t.Fatal("test should start with 0 files")
	}

	// upload a 100 byte file in chunks of 10 bytes.
	chunkSize := 2 * int64(skymodules.ChunkSize(crypto.TypePlain, uint64(skymodules.RenterDefaultDataPieces)))
	fileSize := chunkSize*5 + chunkSize/2 // 5 1/2 chunks.
	uploadedData := fastrand.Bytes(int(fileSize))

	// Get a tus client and upload.
	tc, upload, err := r.SkynetTUSNewUploadFromBytes(uploadedData, chunkSize)
	if err != nil {
		t.Fatal(err)
	}

	// Start upload.
	uploader, err := tc.CreateUpload(upload)
	if err != nil {
		t.Fatal(err)
	}

	// Upload a single chunk.
	err = uploader.UploadChunck()
	if err != nil {
		t.Fatal(err)
	}

	// Wait for two full pruning intervals to make sure pruning ran at least
	// once.
	time.Sleep(2 * renter.PruneTUSUploadTimeout)

	// Upload another chunk.
	err = uploader.UploadChunck()
	if err == nil || !strings.Contains(err.Error(), "404") {
		t.Fatal(err)
	}

	// Try to resume upload.
	uploader, err = tc.ResumeUpload(upload)
	if err == nil || !errors.Contains(err, tus.ErrUploadNotFound) {
		t.Fatal(err)
	}

	// Check that the number of files didn't increase since the new files were
	// purged.
	err = build.Retry(100, 100*time.Millisecond, func() error {
		dir, err = r.RenterDirRootGet(skymodules.SkynetFolder)
		if err != nil {
			return err
		}
		nFiles := dir.Directories[0].AggregateNumFiles
		if nFiles != 0 {
			return fmt.Errorf("expected 0 new files but got %v", nFiles)
		}
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}

// testTUSUploaderUnstableConnection tests uploading with a TUS uploader where
// every chunk upload fails halfway through.
func testTUSUploaderUnstableConnection(t *testing.T, tg *siatest.TestGroup) {
	// Add a custom renter with dependency.
	rp := node.RenterTemplate
	rp.RenterDeps = &dependencies.DependencyUnstableTUSUpload{}
	nodes, err := tg.AddNodes(rp)
	if err != nil {
		t.Fatal(err)
	}
	r := nodes[0]
	defer func() {
		if err := tg.RemoveNode(r); err != nil {
			t.Fatal(err)
		}
	}()

	// Get a tus client.
	chunkSize := 2 * int64(skymodules.ChunkSize(crypto.TypePlain, uint64(skymodules.RenterDefaultDataPieces)))

	// Upload some chunks.
	uploadedData := fastrand.Bytes(int(chunkSize * 10))
	tc, upload, err := r.SkynetTUSNewUploadFromBytes(uploadedData, chunkSize)
	if err != nil {
		t.Fatal(err)
	}

	// Create uploader.
	uploader, err := tc.CreateUpload(upload)
	if err != nil {
		t.Fatal(err)
	}

	// Upload all the chunks. Whenever we encounter an error we resume the
	// upload until we run out of retries.
	remainingChunks := len(uploadedData) / int(chunkSize)
	remainingTries := 5 * remainingChunks
	for remainingChunks > 0 {
		err = uploader.UploadChunck()
		if err == nil {
			remainingChunks--
			continue // continue
		}
		// Decrement remaining tries.
		if remainingTries == 0 {
			t.Fatal("out of retries")
		}
		remainingTries--
		// Resume upload.
		uploader, err = tc.ResumeUpload(upload)
		if err != nil {
			t.Fatal(err)
		}
	}

	// Fetch skylink after upload is done.
	skylink, err := client.SkylinkFromTUSURL(tc, uploader.Url())
	if err != nil {
		t.Fatal(err)
	}

	// Download the uploaded data and compare it to the uploaded data.
	downloadedData, err := r.SkynetSkylinkGet(skylink)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Equal(uploadedData, downloadedData) {
		t.Fatal("data doesn't match")
	}
}

// testTUSUploaderConnectionDropped tests dropping the connection between
// chunks.
func testTUSUploaderConnectionDropped(t *testing.T, tg *siatest.TestGroup) {
	// Add a custom renter with dependency.
	rp := node.RenterTemplate
	deps := dependencies.NewDependencyTUSConnectionDrop()
	rp.RenterDeps = deps
	nodes, err := tg.AddNodes(rp)
	if err != nil {
		t.Fatal(err)
	}
	r := nodes[0]
	defer func() {
		if err := tg.RemoveNode(r); err != nil {
			t.Fatal(err)
		}
	}()

	// Get a tus client.
	chunkSize := int64(skymodules.ChunkSize(crypto.TypePlain, uint64(skymodules.RenterDefaultDataPieces)))

	// Create upload for a file that is 1.5 chunks large.
	uploadedData := fastrand.Bytes(int(3 * chunkSize / 2))
	tc, upload, err := r.SkynetTUSNewUploadFromBytes(uploadedData, chunkSize)

	// Create uploader and upload the first chunk.
	uploader, err := tc.CreateUpload(upload)
	if err != nil {
		t.Fatal(err)
	}
	err = uploader.UploadChunck()
	if err != nil {
		t.Fatal(err)
	}

	// Trigger the failure on the dependency and try to upload the remaining
	// data. That should fail.
	deps.Fail()
	err = uploader.Upload()
	if err == nil {
		t.Fatal("should fail")
	}

	// Pick up upload from where we left off. The offset should be at 1 chunk since
	// we only managed to upload 1 chunk successfully.
	uploader, err = tc.ResumeUpload(upload)
	if err != nil {
		t.Fatal(err)
	}
	if upload.Offset() != chunkSize {
		t.Fatal("wrong offset")
	}
	// Upload remaining data. Should work now.
	err = uploader.Upload()
	if err != nil {
		t.Fatal(err)
	}

	// Fetch skylink after upload is done.
	skylink, err := client.SkylinkFromTUSURL(tc, uploader.Url())
	if err != nil {
		t.Fatal(err)
	}

	// Download the uploaded data and compare it to the uploaded data.
	downloadedData, err := r.SkynetSkylinkGet(skylink)
	if err != nil {
		t.Fatal(err)
	}
	if !bytes.Equal(uploadedData, downloadedData) {
		t.Fatal("data doesn't match")
	}
}

// testTUSUploaderConcat tests the concatenation capabilities of tus uploads.
func testTUSUploaderConcat(t *testing.T, r *siatest.TestNode) {
	// Get the number of files at the beginning of the test.
	dir, err := r.RenterDirRootGet(skymodules.SkynetFolder)
	if err != nil {
		t.Fatal(err)
	}
	nFiles := dir.Directories[0].AggregateNumFiles

	// Prepare the data.
	fullData := fastrand.Bytes(int(modules.SectorSize))
	partialData := fastrand.Bytes(1)

	// Upload 1 - full sector
	var wg sync.WaitGroup
	wg.Add(1)
	var urlFull *partialUpload
	go func() {
		defer wg.Done()
		var err error
		urlFull, err = createPartialUpload(r, uint64(len(fullData)))
		if err != nil {
			t.Error(err)
		}
		err = urlFull.Patch(fullData, 0)
		if err != nil {
			t.Error(err)
		}
	}()

	// Upload 2 - partial sector
	wg.Add(1)
	var urlPartial *partialUpload
	go func() {
		defer wg.Done()
		var err error
		urlPartial, err = createPartialUpload(r, uint64(len(partialData)))
		if err != nil {
			t.Error(err)
		}
		err = urlPartial.Patch(partialData, 0)
		if err != nil {
			t.Error(err)
		}
	}()
	wg.Wait()

	// Skip remaining test if we already failed.
	if t.Failed() {
		t.SkipNow()
	}

	// Concat them.
	urlConcat, err := concatPartialUploads(r, urlFull, urlPartial)
	if err != nil {
		t.Fatal(err)
	}
	sl, err := r.SkylinkFromTUSID(urlConcat)
	if err != nil {
		t.Fatal(err)
	}

	// Download the skylink.
	downloaded, err := r.SkynetSkylinkGet(sl)
	if err != nil {
		t.Fatal(err)
	}
	expected := append(fullData, partialData...)
	if !bytes.Equal(downloaded, expected) {
		t.Fatal("data mismatch", len(downloaded), len(expected))
	}

	// Concat them the wrong way round. This should not work.
	_, err = concatPartialUploads(r, urlPartial, urlFull)
	if err == nil {
		t.Fatal("shouldn't work")
	}

	// Wait for two full pruning intervals to make sure pruning ran at least
	// once.
	time.Sleep(2 * renter.PruneTUSUploadTimeout)

	dir, err = r.RenterDirRootGet(skymodules.SkynetFolder)
	if err != nil {
		t.Fatal(err)
	}
	nFilesAfter := dir.Directories[0].AggregateNumFiles
	if nFilesAfter-nFiles != 3 {
		t.Fatal("expected 3 .sia files to be created for the 2 parts but got", nFilesAfter-nFiles)
	}

	// Try to concat a TUS file that was uploaded regularly. This should
	// fail.
	client, regularUpload, err := r.SkynetTUSNewUploadFromBytes([]byte{1}, int64(modules.SectorSize))
	if err != nil {
		t.Fatal(err)
	}
	uploader, err := client.CreateUpload(regularUpload)
	if err != nil {
		t.Fatal(err)
	}
	err = uploader.Upload()
	if err != nil {
		t.Fatal(err)
	}
	// Concat them.
	regularUploadURL, found := client.Config.Store.Get(regularUpload.Fingerprint)
	if !found {
		t.Fatal("url not found")
	}
	regularPartialUpload, err := newPartialUpload(regularUploadURL)
	if err != nil {
		t.Fatal(err)
	}
	_, err = concatPartialUploads(r, regularPartialUpload, regularPartialUpload)
	if err == nil {
		t.Fatal("should fail")
	}
}

// TestSkynetResumeOnSeparatePortal tests uploading a chunk to one portal and
// finishing the upload on another portal.
func TestSkynetResumeOnSeparatePortal(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}
	// NOTE: Don't run this test in parallel with other tests to make sure
	// we can drop the collection

	// Connect to db directly.
	uri, ok := build.MongoDBURI()
	if !ok {
		t.Fatal("uri not set")
	}
	opts := options.Client().
		ApplyURI(uri).
		SetAuth(mongoTestCreds)

	client, err := mongo.Connect(context.Background(), opts)
	if err != nil {
		t.Fatal(err)
	}
	defer client.Disconnect(context.Background())

	// Clear collection before the test.
	collection := client.Database(renter.TusDBName).Collection(renter.TusUploadsMongoCollectionName)
	if err := collection.Drop(context.Background()); err != nil {
		t.Fatal(err)
	}

	// Prepare a testgroup.
	groupParams := siatest.GroupParams{
		Hosts:  3,
		Miners: 1,
	}
	groupDir := skynetTestDir(t.Name())
	tg, err := siatest.NewGroupFromTemplate(groupDir, groupParams)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := tg.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	// Create 2 portals with a mongo connection.
	rt := node.RenterTemplate
	rt.CreatePortal = true
	rt.MongoUploadStoreURI = uri
	rt.MongoUploadStoreCreds = mongoTestCreds
	rtA, rtB := rt, rt
	rtA.MongoUploadStoreServerUID = "A"
	rtB.MongoUploadStoreServerUID = "B"
	portals, err := tg.AddNodes(rtA, rtB)
	if err != nil {
		t.Fatal(err)
	}
	portalA, portalB := portals[0], portals[1]

	// Prepare 3 chunks of upload data.
	numChunks := 3
	chunkSize := int64(numChunks) * int64(skymodules.ChunkSize(crypto.TypePlain, uint64(skymodules.RenterDefaultDataPieces)))
	uploadData := fastrand.Bytes(int(chunkSize * 3))

	// Create uploader.
	tcA, upload, err := portalA.SkynetTUSNewUploadFromBytes(uploadData, chunkSize)
	if err != nil {
		t.Fatal(err)
	}
	uploaderA, err := tcA.CreateUpload(upload)
	if err != nil {
		t.Fatal(err)
	}

	// Upload a chunk to portal A.
	if err := uploaderA.UploadChunck(); err != nil {
		t.Fatal(err)
	}

	// Both portal should not be able to provide a skylink yet.
	urlA, found := tcA.Config.Store.Get(upload.Fingerprint)
	if !found {
		t.Fatal("url should exist")
	}
	uploadID := filepath.Base(urlA)
	_, err = portalA.SkylinkFromTUSID(uploadID)
	if err == nil {
		t.Fatal(err)
	}
	_, err = portalB.SkylinkFromTUSID(uploadID)
	if err == nil {
		t.Fatal(err)
	}

	// Create a new client for portal B.
	tcB, err := portalB.SkynetTUSClient(chunkSize)
	if err != nil {
		t.Fatal(err)
	}

	// Store the upload's url in tcB's storage for 'ResumeUpload' to work.
	urlB := fmt.Sprintf("%s/%s", tcB.Url, uploadID)
	tcB.Config.Store.Set(upload.Fingerprint, urlB)

	// Finish the upload on portal B.
	uploaderB, err := tcB.ResumeUpload(upload)
	if err != nil {
		t.Fatal(err)
	}
	if err := uploaderB.Upload(); err != nil {
		t.Fatal(err)
	}

	// Both portal should be able to provide a skylink and download it.
	skylinkA, err := portalA.SkylinkFromTUSID(uploadID)
	if err != nil {
		t.Fatal(err)
	}
	skylinkB, err := portalB.SkylinkFromTUSID(uploadID)
	if err != nil {
		t.Fatal(err)
	}

	dataA, err := portalA.SkynetSkylinkGet(skylinkA)
	if err != nil {
		t.Fatal(err)
	}
	dataB, err := portalB.SkynetSkylinkGet(skylinkB)
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(dataA, uploadData) {
		t.Fatal("dataA mismatch")
	}
	if !bytes.Equal(dataB, uploadData) {
		t.Fatal("dataB mismatch")
	}

	// Get all uploads.
	c, err := collection.Find(context.Background(), bson.M{})
	if err != nil {
		t.Fatal(err)
	}
	var uploads []renter.MongoTUSUpload
	for c.Next(context.Background()) {
		var upload renter.MongoTUSUpload
		err = c.Decode(&upload)
		if err != nil {
			t.Fatal(err)
		}
		uploads = append(uploads, upload)
	}

	// Should have 1 upload.
	if len(uploads) != 1 {
		for _, u := range uploads {
			t.Log(u.ID, u.ServerNames)
		}
		t.Fatal("expected 1 upload got", len(uploads))
	}
	u := uploads[0]

	// Upload should contain both portal names.
	if len(u.ServerNames) != 2 {
		t.Fatal("expected 2 portals got", u.ServerNames)
	}
	serverUIDs := strings.Join(u.ServerNames, "")
	if serverUIDs != "AB" && serverUIDs != "BA" {
		t.Fatal("wrong portal names", u.ServerNames)
	}

	// Drop uploads at the end of the test.
	if err := collection.Drop(context.Background()); err != nil {
		t.Fatal(err)
	}
}

// testTrustlessUpload makes sure the test suit is trustlessly verifying the
// returned skylink during uploads.
func testTrustlessUpload(t *testing.T, tg *siatest.TestGroup) {
	rp := node.RenterTemplate
	nodes, err := tg.AddNodes(rp)
	if err != nil {
		t.Fatal(err)
	}
	r := nodes[0]
	defer func() {
		if err := tg.RemoveNode(r); err != nil {
			t.Fatal(err)
		}
	}()

	// Get a tus client.
	chunkSize := 2 * int64(skymodules.ChunkSize(crypto.TypePlain, uint64(skymodules.RenterDefaultDataPieces)))

	// Create custom metadata.
	uploadedData := []byte{1, 2, 3}
	customSM := skymodules.SkyfileMetadata{
		Filename: "foo",
		Length:   uint64(len(uploadedData)),
		Subfiles: skymodules.SkyfileSubfiles{
			"bar": skymodules.SkyfileSubfileMetadata{
				Filename: "bar",
				Len:      uint64(len(uploadedData)),
			},
		},
	}
	smBytes, err := json.Marshal(customSM)
	if err != nil {
		t.Fatal(err)
	}

	// Precompute the expected skylink before doing the upload.
	expectedSkylink, err := client.TusSkylink(smBytes, uploadedData)
	if err != nil {
		t.Fatal(err)
	}

	// Upload. The filename and filetype should both be ignored and not
	// contribute to the skylink.
	randStr := hex.EncodeToString(fastrand.Bytes(16))
	skylink, err := r.SkynetTUSUploadCustom(uploadedData, chunkSize, randStr, randStr, math.MaxInt64, smBytes)
	if err != nil {
		t.Fatal(err)
	}

	// The skylink should match.
	if skylink != expectedSkylink.String() {
		t.Fatal("Unexpected skylink", skylink, expectedSkylink)
	}

	// Try again. This time we corrupt the metadata to be invalid. In this
	// case by changing the subfile's string in the map to not match the
	// filename.
	customSM.Subfiles["bar"] = skymodules.SkyfileSubfileMetadata{
		Filename: "mismatch",
	}
	smBytes, err = json.Marshal(customSM)
	if err != nil {
		t.Fatal(err)
	}
	_, err = r.SkynetTUSUploadCustom(uploadedData, chunkSize, randStr, randStr, math.MaxInt64, smBytes)
	if err == nil || !strings.Contains(err.Error(), "unexpected status code: 500") {
		t.Fatal("Should fail")
	}
}

// testTUSUploaderConcatSeparateServers is a test for the edge case of every
// partial upload existing on a separate server and the finalizing server also
// being a separate server.
func testTUSUploaderConcatSeparateServers(t *testing.T, tg *siatest.TestGroup, rt node.NodeParams) {
	// Create a few portals. One for uploading each partial upload to and
	// one to concat them at.
	portals, err := tg.AddNodeN(rt, 3)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := tg.RemoveNodeN(portals...); err != nil {
			t.Fatal(err)
		}
	}()
	portalUp1, portalUp2, portalConcat := portals[0], portals[1], portals[2]

	// Declare the chunkSize.
	chunkSize := uint64(skymodules.ChunkSize(crypto.TypePlain, uint64(skymodules.RenterDefaultDataPieces)))

	// Create the partial uploads.
	puA, err := createPartialUpload(portalUp1, chunkSize)
	if err != nil {
		t.Fatal(err)
	}
	puB, err := createPartialUpload(portalUp2, chunkSize)
	if err != nil {
		t.Fatal(err)
	}

	// Upload one chunk to each.
	chunk1 := fastrand.Bytes(int(chunkSize))
	err = puA.Patch(chunk1, 0)
	if err != nil {
		t.Fatal(err)
	}
	chunk2 := fastrand.Bytes(int(chunkSize))
	err = puB.Patch(chunk2, 0)
	if err != nil {
		t.Fatal(err)
	}

	// Concat them on the third portal.
	uploadID, err := concatPartialUploads(portalConcat, puA, puB)
	if err != nil {
		t.Fatal(err)
	}
	chunkData := append(chunk1, chunk2...)

	// Should be able to download it from all three.
	downloadedSkylinks := make(map[string]struct{})
	for _, portal := range portals {
		skylink, err := portal.SkylinkFromTUSID(uploadID)
		if err != nil {
			t.Fatal(err)
		}
		data, err := portal.SkynetSkylinkGet(skylink)
		if err != nil {
			t.Fatal(err)
		}
		if !bytes.Equal(data, chunkData) {
			t.Fatal("byte mismatch", len(data), len(chunkData))
		}
		downloadedSkylinks[skylink] = struct{}{}
	}

	// They should all have resolved to the same link.
	if len(downloadedSkylinks) != 1 {
		t.Fatal("unexpected number of skylinks", len(downloadedSkylinks))
	}

	// Portal1 should not report the skylink for its partial piece.
	skylinks, err := portalUp1.PinnedSkylinks()
	if err != nil {
		t.Fatal(err)
	}
	if len(skylinks) != 0 {
		t.Fatal("shouldn't have any skylinks", len(skylinks))
	}

	// Portal2 should not report the skylink for its partial piece.
	skylinks, err = portalUp2.PinnedSkylinks()
	if err != nil {
		t.Fatal(err)
	}
	if len(skylinks) != 0 {
		t.Fatal("shouldn't have any skylinks", len(skylinks))
	}

	// Portal3 should report the skylink 2 times. Once for the base sector
	// .sia file and once for the extension.
	skylinks, err = portalConcat.PinnedSkylinks()
	if err != nil {
		t.Fatal(err)
	}
	if len(skylinks) != 2 {
		t.Log(skylinks)
		t.Fatal("should have exactly 1 skylink", len(skylinks))
	}
	if _, ok := downloadedSkylinks[skylinks[0].String()]; !ok {
		t.Fatal("unknown skylink found")
	}
	if _, ok := downloadedSkylinks[skylinks[1].String()]; !ok {
		t.Fatal("unknown skylink found")
	}
}
