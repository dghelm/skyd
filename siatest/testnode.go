package siatest

import (
	"net"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"testing"
	"time"

	"gitlab.com/NebulousLabs/errors"

	"gitlab.com/SkynetLabs/skyd/build"
	"gitlab.com/SkynetLabs/skyd/node"
	"gitlab.com/SkynetLabs/skyd/node/api"
	"gitlab.com/SkynetLabs/skyd/node/api/client"
	"gitlab.com/SkynetLabs/skyd/node/api/server"
	"gitlab.com/SkynetLabs/skyd/skymodules"
	"go.sia.tech/siad/modules"
	"go.sia.tech/siad/persist"
	"go.sia.tech/siad/types"
)

var (
	// testNodeAddressCounter is a global variable that tracks the counter for
	// the test node addresses for all tests. It starts at 127.1.0.0 and
	// iterates through the entire range of 127.X.X.X above 127.1.0.0
	testNodeAddressCounter = newNodeAddressCounter()
)

type (
	// TestNode is a helper struct for testing that contains a server and a
	// client as embedded fields.
	TestNode struct {
		*server.Server
		client.Client
		params      node.NodeParams
		primarySeed string

		downloadDir *LocalDir
		filesDir    *LocalDir
	}

	// addressCounter is a help struct for assigning new addresses to test nodes
	addressCounter struct {
		address net.IP
		mu      sync.Mutex
	}
)

// newNodeAddressCounter creates a new address counter and returns it. The
// counter will cover the entire range 127.X.X.X above 127.1.0.0
//
// The counter is initialized with an IP address of 127.1.0.0 so that testers
// can manually add nodes in the address range of 127.0.X.X without causing a
// conflict
func newNodeAddressCounter() *addressCounter {
	counter := &addressCounter{
		address: net.IPv4(127, 1, 0, 0),
	}
	return counter
}

// managedNextNodeAddress returns the next node address from the addressCounter
func (ac *addressCounter) managedNextNodeAddress() (string, error) {
	ac.mu.Lock()
	defer ac.mu.Unlock()
	// IPs are byte slices with a length of 16, the IPv4 address range is stored
	// in the last 4 indexes, 12-15
	for i := len(ac.address) - 1; i >= 12; i-- {
		if i == 12 {
			return "", errors.New("ran out of IP addresses")
		}
		ac.address[i]++
		if ac.address[i] > 0 {
			break
		}
	}

	// If mac return 127.0.0.1
	if runtime.GOOS == "darwin" {
		return "127.0.0.1", nil
	}

	return ac.address.String(), nil
}

// NewNode creates a new funded TestNode
func NewNode(nodeParams node.NodeParams) (*TestNode, error) {
	// We can't create a funded node without a miner
	if !nodeParams.CreateMiner && nodeParams.Miner == nil {
		return nil, errors.New("Can't create funded node without miner")
	}
	// Create clean node
	tn, err := NewCleanNode(nodeParams)
	if err != nil {
		return nil, err
	}
	// Fund the node
	for i := types.BlockHeight(0); i <= types.MaturityDelay+types.TaxHardforkHeight; i++ {
		if err := tn.MineBlock(); err != nil {
			return nil, err
		}
	}
	// Return TestNode
	return tn, nil
}

// NewCleanNode creates a new TestNode that's not yet funded
func NewCleanNode(nodeParams node.NodeParams) (*TestNode, error) {
	return newCleanNode(nodeParams, false)
}

// NewCleanNodeAsync creates a new TestNode that's not yet funded
func NewCleanNodeAsync(nodeParams node.NodeParams) (*TestNode, error) {
	return newCleanNode(nodeParams, true)
}

// newCleanNode creates a new TestNode that's not yet funded
func newCleanNode(nodeParams node.NodeParams, asyncSync bool) (*TestNode, error) {
	userAgent := "Sia-Agent"
	password := "password"

	// Check if an RPC address is set
	if nodeParams.RPCAddress == "" {
		addr, err := testNodeAddressCounter.managedNextNodeAddress()
		if err != nil {
			return nil, errors.AddContext(err, "error getting next node address")
		}
		nodeParams.RPCAddress = addr + ":0"
	}

	// Check if the SiaMuxAddress is set, if not we want to set it to use a
	// random port in testing
	if nodeParams.SiaMuxTCPAddress == "" {
		nodeParams.SiaMuxTCPAddress = "localhost:0"
	}
	if nodeParams.SiaMuxWSAddress == "" {
		nodeParams.SiaMuxWSAddress = "localhost:0"
	}

	// Create server
	var s *server.Server
	var err error
	if asyncSync {
		var errChan <-chan error
		s, errChan = server.NewAsync(":0", userAgent, password, nodeParams, time.Now())
		err = modules.PeekErr(errChan)
	} else {
		s, err = server.New(":0", userAgent, password, nodeParams, time.Now())
	}
	if err != nil {
		return nil, err
	}

	// Create client
	opts := client.Options{
		Address:   s.APIAddress(),
		Password:  password,
		UserAgent: userAgent,
	}
	c := client.New(opts)

	// Create TestNode
	tn := &TestNode{
		Server:      s,
		Client:      *c,
		params:      nodeParams,
		primarySeed: "",
	}
	if err = tn.initRootDirs(); err != nil {
		return nil, errors.AddContext(err, "failed to create root directories")
	}

	// Remember the RPC address.
	tn.params.RPCAddress = string(tn.GatewayAddress())

	// Check if the node is a host. If it is, we remember its siamux
	// address.
	if nodeParams.CreateHost || nodeParams.Host != nil {
		hg, err := tn.HostGet()
		if err != nil {
			return nil, err
		}
		es := hg.ExternalSettings
		tn.params.HostAddress = string(es.NetAddress)
		tn.params.SiaMuxTCPAddress = es.SiaMuxAddress()
	}

	// If there is no wallet we are done.
	if !nodeParams.CreateWallet && nodeParams.Wallet == nil {
		return tn, nil
	}

	// If the SkipWalletInit flag is set then we are done
	if nodeParams.SkipWalletInit {
		return tn, nil
	}

	// Init wallet
	if nodeParams.PrimarySeed != "" {
		err := tn.WalletInitSeedPost(nodeParams.PrimarySeed, "", false)
		if err != nil {
			return nil, err
		}
		tn.primarySeed = nodeParams.PrimarySeed
	} else {
		wip, err := tn.WalletInitPost("", false)
		if err != nil {
			return nil, err
		}
		tn.primarySeed = wip.PrimarySeed
	}

	// Unlock wallet
	if err := tn.WalletUnlockPost(tn.primarySeed); err != nil {
		return nil, err
	}

	// Return TestNode
	return tn, nil
}

// IsAlertRegistered returns an error if the given alert is not found
func (tn *TestNode) IsAlertRegistered(a modules.Alert) error {
	return build.Retry(10, 100*time.Millisecond, func() error {
		dag, err := tn.DaemonAlertsGet()
		if err != nil {
			return err
		}
		for _, alert := range dag.Alerts {
			if alert.Equals(a) {
				return nil
			}
		}
		return errors.New("alert is not registered")
	})
}

// IsAlertUnregistered returns an error if the given alert is still found
func (tn *TestNode) IsAlertUnregistered(a modules.Alert) error {
	return build.Retry(10, 100*time.Millisecond, func() error {
		dag, err := tn.DaemonAlertsGet()
		if err != nil {
			return err
		}

		for _, alert := range dag.Alerts {
			if alert.Equals(a) {
				return errors.New("alert is registered")
			}
		}
		return nil
	})
}

// PinnedSkylinks searches the node for skylinks associated with any .sia files
// and returns them. The list may contain duplicates if the skylink appears more
// than once to avoid hiding edge cases in testing and be able to test pinning
// more explicitly than simply using the /skynet/skylinks endpoint.
func (tn *TestNode) PinnedSkylinks() (skylinks []skymodules.Skylink, _ error) {
	return tn.recursivePinnedSkylinks(skymodules.RootSiaPath())
}

// recursivePinnedSkylinks recursively searches the node for skylinks associated
// with any .sia files and returns them. The list may contain duplicates if the
// skylink appears more than once to avoid hiding edge cases.
func (tn *TestNode) recursivePinnedSkylinks(sp skymodules.SiaPath) (skylinks []skymodules.Skylink, _ error) {
	dir, err := tn.RenterDirRootGet(sp)
	if err != nil {
		return nil, err
	}
	for _, file := range dir.Files {
		for _, skylink := range file.Skylinks {
			var sl skymodules.Skylink
			err = sl.LoadString(skylink)
			if err != nil {
				return nil, err
			}
			skylinks = append(skylinks, sl)
		}
	}
	for _, dir := range dir.Directories[1:] {
		sls, err := tn.recursivePinnedSkylinks(dir.SiaPath)
		if err != nil {
			return nil, err
		}
		skylinks = append(skylinks, sls...)
	}
	return
}

// PrintDebugInfo prints out helpful debug information when debug tests and ndfs, the
// boolean arguments dictate what is printed
func (tn *TestNode) PrintDebugInfo(t *testing.T, contractInfo, hostInfo, renterInfo bool) {
	if contractInfo {
		printContractInfo := func(contracts []api.RenterContract) {
			for _, c := range contracts {
				t.Log("    ID", c.ID)
				t.Log("    HostPublicKey", c.HostPublicKey)
				t.Log("    GoodForUpload", c.GoodForUpload)
				t.Log("    GoodForRenew", c.GoodForRenew)
				t.Log("    EndHeight", c.EndHeight)
				t.Log("    Size", c.Size)
			}
		}
		rc, err := tn.RenterAllContractsGet()
		if err != nil {
			t.Log(err)
		}
		t.Log("Active Contracts")
		printContractInfo(rc.ActiveContracts)
		t.Log()

		t.Log("Passive Contracts")
		printContractInfo(rc.PassiveContracts)
		t.Log()

		t.Log("Refreshed Contracts")
		printContractInfo(rc.RefreshedContracts)
		t.Log()

		t.Log("Disabled Contracts")
		printContractInfo(rc.DisabledContracts)
		t.Log()

		t.Log("Expired Contracts")
		printContractInfo(rc.ExpiredContracts)
		t.Log()

		t.Log("Expired Refreshed Contracts")
		printContractInfo(rc.ExpiredRefreshedContracts)
		t.Log()
	}

	if hostInfo {
		hdbag, err := tn.HostDbAllGet()
		if err != nil {
			t.Log(err)
		}
		t.Log("All Hosts from HostDB")
		for _, host := range hdbag.Hosts {
			hostInfo, err := tn.HostDbHostsGet(host.PublicKey)
			if err != nil {
				t.Log(err)
			}
			t.Log("    Host:", host.NetAddress)
			t.Log("        score", hostInfo.ScoreBreakdown.Score)
			t.Log("        breakdown", hostInfo.ScoreBreakdown)
			t.Log("        pk", host.PublicKey)
			t.Log("        Accepting Contracts", host.HostExternalSettings.AcceptingContracts)
			t.Log("        Filtered", host.Filtered)
			t.Log("        LastIPNetChange", host.LastIPNetChange.String())
			t.Log("        Subnets")
			for _, subnet := range host.IPNets {
				t.Log("            ", subnet)
			}
			t.Log()
		}
		t.Log()
	}

	if renterInfo {
		t.Log("Renter Info")
		rg, err := tn.RenterGet()
		if err != nil {
			t.Log(err)
		}
		t.Log("CP:", rg.CurrentPeriod)
		cg, err := tn.ConsensusGet()
		if err != nil {
			t.Log(err)
		}
		t.Log("BH:", cg.Height)
		settings := rg.Settings
		t.Log("Allowance Funds:", settings.Allowance.Funds.HumanString())
		fm := rg.FinancialMetrics
		t.Log("Unspent Funds:", fm.Unspent.HumanString())
		t.Log()
	}
}

// RestartNode restarts a TestNode
func (tn *TestNode) RestartNode() error {
	err := tn.StopNode()
	if err != nil {
		return errors.AddContext(err, "Could not stop node")
	}
	err = tn.StartNode()
	if err != nil {
		return errors.AddContext(err, "Could not start node")
	}
	return nil
}

// SiaPath returns the siapath of a local file or directory to be used for
// uploading
func (tn *TestNode) SiaPath(path string) skymodules.SiaPath {
	s := strings.TrimPrefix(path, tn.filesDir.path+string(filepath.Separator))
	sp, err := skymodules.NewSiaPath(s)
	if err != nil {
		build.Critical("This shouldn't happen", err)
	}
	return sp
}

// StartNode starts a TestNode from an active group
func (tn *TestNode) StartNode() error {
	// Create server
	s, err := server.New(tn.Client.Address, tn.UserAgent, tn.Password, tn.params, time.Now())
	if err != nil {
		return err
	}
	tn.Server = s
	tn.Client.Address = s.APIAddress()
	if !tn.params.CreateWallet && tn.params.Wallet == nil {
		return nil
	}
	return tn.WalletUnlockPost(tn.primarySeed)
}

// StartNodeCleanDeps restarts a node from an active group without its
// previously assigned dependencies.
func (tn *TestNode) StartNodeCleanDeps() error {
	tn.params.ConsensusSetDeps = nil
	tn.params.ContractorDeps = nil
	tn.params.ContractSetDeps = nil
	tn.params.GatewayDeps = nil
	tn.params.HostDeps = nil
	tn.params.HostDBDeps = nil
	tn.params.RenterDeps = nil
	tn.params.TPoolDeps = nil
	tn.params.WalletDeps = nil
	return tn.StartNode()
}

// StopNode stops a TestNode
func (tn *TestNode) StopNode() error {
	return errors.AddContext(tn.Close(), "failed to stop node")
}

// initRootDirs creates the download and upload directories for the TestNode
func (tn *TestNode) initRootDirs() error {
	tn.downloadDir = &LocalDir{
		path: filepath.Join(tn.RenterDir(), "downloads"),
	}
	if err := os.MkdirAll(tn.downloadDir.path, persist.DefaultDiskPermissionsTest); err != nil {
		return err
	}
	tn.filesDir = &LocalDir{
		path: filepath.Join(tn.RenterDir(), "uploads"),
	}
	if err := os.MkdirAll(tn.filesDir.path, persist.DefaultDiskPermissionsTest); err != nil {
		return err
	}
	return nil
}
